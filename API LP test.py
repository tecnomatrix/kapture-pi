#!usr/bin/python
#
# This is an example of how to use the LiquidPlanner API in Python.
#

# You need the Requests library, found at:
# http://docs.python-requests.org/en/latest/index.html
# note that Requests version 1.0 or later is required
#
import requests
import csv
import json
import getpass
import pyodbc
import pandas as pd


# Comentario de developer


class LiquidPlanner:

    base_uri     = 'https://app.liquidplanner.com/api'
    workspace_id = None
    email        = None
    password     = None
    session      = None

    def __init__( self, email, password ):
        self.email    = email
        self.password = password
  
    def get_workspace_id( self ):
        return self.workspace_id
    
    def set_workspace_id( self, workspace_id ):
        self.workspace_id = workspace_id
  
    def get( self, uri, options={} ):
        return requests.get( self.base_uri + uri, 
          data    = options,
          headers = { 'Content-Type': 'application/json' },
          auth    = ( self.email, self.password )
        )
    
    def post( self, uri, options={} ):
        return requests.post( self.base_uri + uri, 
          data    = options,
          headers = { 'Content-Type': 'application/json' },
          auth    = ( self.email, self.password )
        )
    
    def put( self, uri, options={} ):
        return requests.put( self.base_uri + uri, 
          data    = options,
          headers = { 'Content-Type': 'application/json' },
          auth    = ( self.email, self.password )
        )
  
    # returns a dictionary with information about the current user  
    def account( self ):
        return self.get( '/account' ).json()
  
    # returns a list of dictionaries, each a workspace in which this user is a member
    def workspaces( self ):
        return self.get( '/workspaces' ).json()
    
    # returns a list of dictionaries, each a workspace in which this user is a member
#    def TrackTimePOST( self ):
#        #https://app.liquidplanner.com/api/v1/workspaces/:id/tasks/:id/track_time \
#       #-d '{ "work":"4", "low": "1.5d", "high": "3d", "activity_id":12345}'
#        return self.get( '/v1/workspaces/204560/tasks/296223/track_time?append=false ' ).json()
    
#    def TrackTimePost(self):
#            return requests.post(self.base_uri + '/v1/workspaces/204560/tasks/296223/track_time?append=false ',
#                    data = {"member_id": 959109, "work":"4", "activity_id":296223, "work_performed_on": "2019-07-26"},
#                    headers = { 'Content-Type': 'application/json' },
#                    auth    = ( self.email, self.password ))
    
    # returns a list of dictionaries, each a project in a workspace  
    def projects( self ):
        return self.get( '/workspaces/' + str(self.workspace_id) +
                         '/projects' ).json()
  
    # returns a list of dictionaries, each a task in a workspace  
    def tasks( self ):
        return self.get( '/workspaces/' + str(self.workspace_id) +
                         '/tasks' ).json()
        
   # returns a list of dictionaries, each a task in a workspace  
    def members( self ):
        return self.get( '/workspaces/' + str(self.workspace_id) +
                         '/members' ).json()
        
    # creates a task by POSTing data
    def create_task( self, data ):
        return self.post( '/workspaces/' + str(self.workspace_id) +
                          '/tasks', json.dumps({ 'task': data }) ).json()
    
    # updates a task by PUTing data
    def update_task( self, data ):
        return self.put( '/workspaces/' + str(self.workspace_id) +
                         '/tasks/' + str(data['id']), 
                         json.dumps({ 'task' : data }) ).json()

    # demonstrates usage of the LP class
    @staticmethod
    def demo():
#        email    = input( "Enter your e-mail address: " )
#        password = getpass.getpass( "Enter your password: " )
#        add_task = 'Y' == input( "Add a task? (Y for yes): " )[0].upper()
        add_task='N'
        email="Xavier.conesa@tecnomatrix.net"
        password="000000008"
        LP = LiquidPlanner( email, password )

        workspace = LP.workspaces()[ 0 ]
        print( "Defaulting to your first workspace, '%s'..." % ( workspace[ 'name' ] ) )
        LP.set_workspace_id( workspace[ 'id' ] )
        print (" Workspace Id: ", workspace[ 'id' ] )

        print( "Here is a list of all of your projects:" )
        projects = LP.projects()
        header=['parent_crumbs','name','id','is_done','latest_finish','hours_logged','expected_finish','promise_by']
        LP.writeCSVProjects('Projects_LiquidPlanner.csv', projects, header)
        for project in projects:
            print( project[ 'name' ], "  - Id:", project[ 'work' ]  )
            
        members=LP.members()
        header=['access_level','email','id','user_name','is_virtual']
        LP.writeCSVMembers('Members_LiquidPlanner.csv',members,header)
        print ('Members:---------')
        

        if add_task:
            print( "Adding a new task to your first project..." )
            project_id = projects[ 0 ][ 'id' ]
            new_task = LP.create_task({ 
              "name"      : "learn the API",
              "parent_id" : int(project_id) 
            })
            print( "Added task %(name)s with id %(id)s" % new_task )
        else:
            print( "Skipped adding a new task" )

        print( "Here is a list of your tasks:" )
        
        tasks = LP.tasks()
        header=['name','activity_id','id','project_id','workspace','person_id']
        LP.writeCSVTask('Task_LiquidPlanner.csv',tasks,header)
        for task in tasks:
            print( task[ 'name' ], " id: ", task[ 'activity_id' ] )
        
#        tracktimes=LP.TrackTime()
        print("----------------------------------------------------------")
#        timeTrackExec=TrackTimePost()
        print("----------------------------------------------------------")
        
    @staticmethod   
    def readCSV(rCSV):
        
             datos=[]
             dirURL=[]
             with open(rCSV) as csvFile:
                 fila=csv.DictReader(csvFile,delimiter='#', quoting=csv.QUOTE_NONE)
                 for reg in fila:
                     datos.append(reg)
                 print(datos[0]['Fecha'])
             return datos
    
    @staticmethod   
    def writeCSVTask(rCSV, datosArray,header):
            myFile_Dades = open('Task_LiquidPlanner.csv', 'a+',newline='',encoding='utf-8')
            writer = csv.DictWriter(myFile_Dades , fieldnames=header, delimiter=',', quoting=csv.QUOTE_NONE) 
            writer.writeheader()
#            writer.writerows(datosArray)
            for datoArray in datosArray:
#            header=['name','activity_id','id','project_id','workspace','person_id']
                writer.writerow(
                        {'name' : datoArray[ 'name' ],
                         'activity_id': datoArray[ 'activity_id' ], 
                         'id':datoArray[ 'id' ],
                         'project_id': datoArray[ 'project_id' ],
                         'workspace': datoArray['space_id'],
                         'person_id': datoArray['assignments'][0]['person_id']
                         }
                        )
#                writer.writerow({'activity_id': datoArray[ 'activity_id' ] })
            myFile_Dades.close()
            
    @staticmethod 
    def writeCSVProjects(rCSV, datosArray, header):
            myFile_Dades = open(rCSV, 'a+',newline='',encoding='utf-8')
            writer = csv.DictWriter(myFile_Dades , fieldnames=header, delimiter=',', quoting=csv.QUOTE_NONE) 
            writer.writeheader()
#            writer.writerows(datosArray)
            for datoArray in datosArray:
#            header=['parent_crumbs','name','id','is_done','latest_finish','hours_logged','expected_finish']
                writer.writerow(
                        {'parent_crumbs' : datoArray[ 'parent_crumbs' ],
                         'name': datoArray[ 'name' ], 
                         'id':datoArray[ 'id' ],
                         'is_done': datoArray[ 'is_done' ],
                         'latest_finish': datoArray['latest_finish'],
                         'hours_logged': datoArray['assignments'][0]['hours_logged'],
                         'expected_finish':datoArray['assignments'][0]['expected_finish'],
                         'promise_by': datoArray['promise_by']}
                        )
#                writer.writerow({'activity_id': datoArray[ 'activity_id' ] })
            myFile_Dades.close()

    @staticmethod 
    def writeCSVMembers(rCSV, datosArray, header):
            myFile_Dades = open(rCSV, 'a+',newline='',encoding='utf-8')
            writer = csv.DictWriter(myFile_Dades , fieldnames=header, delimiter=',', quoting=csv.QUOTE_NONE) 
            writer.writeheader()
#            writer.writerows(datosArray)
            for datoArray in datosArray:
#            header=['access_level','email','id','user_name','is_virtual']
                writer.writerow(
                        {'access_level' : datoArray[ 'access_level' ],
                         'email': datoArray[ 'email' ], 
                         'id':datoArray[ 'id' ],
                         'user_name': datoArray[ 'user_name' ],
                         'is_virtual': datoArray['is_virtual'],
                         }
                        )
#                writer.writerow({'activity_id': datoArray[ 'activity_id' ] })
            myFile_Dades.close()
            
    @staticmethod 
    def imputarHores():
        url = "https://app.liquidplanner.com/api/v1/workspaces/204560/tasks/53083322/track_time"

        querystring = {"append":"false","member_id":"957938","work":"100.5","activity_id":"296223","work_performed_on":"2019-07-16"}

        payload = ""
        headers = {
                'Access-Control-Request-Headers': "'Content-Type': 'application/json'",
                'Content-Type': "application/x-www-form-urlencoded",
                'Authorization': "Basic WGF2aWVyLmNvbmVzYUB0ZWNub21hdHJpeC5uZXQ6MDAwMDAwMDA4",
                'cache-control': "no-cache",
                'Postman-Token': "f94f7bbe-2183-4ffe-9411-c5c35f9fa64b"
                }

        response = requests.request("POST", url, data=payload, headers=headers, params=querystring)
        print(response.text)
        
        h=LiquidPlanner.readCSV('LQ_Export_TrackTime.txt')
        print(h[0]['Fecha'])
        anteriorTask=h[0]['id']
        idTask=anteriorTask
        url = "https://app.liquidplanner.com/api/v1/workspaces/204560/tasks/"+idTask +"/track_time"
        i=0
#        querystring = {"append":"false",
#                       "member_id":h[0]['person_id'],
#                       "work":h[0]['sngDuracio'],
#                       "activity_id":"296223",
#                       "work_performed_on":h[0]['Fecha']}
        for reg in h:
            if reg['id']!=anteriorTask:
                idTask=reg['id']
                url = "https://app.liquidplanner.com/api/v1/workspaces/204560/tasks/"+idTask +"/track_time"
                print(url)
                anteriorTask=idTask
#                i=i+1
#            print(url)
            querystring = {"append":"false",
                       "member_id":reg['person_id'],
                       "work":reg['sngDuracio'],
                       "activity_id":"296223",
                       "work_performed_on":reg['Fecha'],
                       "is_done":"false"}
            response = requests.request("POST", url, data=payload, headers=headers, params=querystring)
            print(str(i) + " - " + str(response.status_code))
            i=i+1
            
    @staticmethod 
    def crearProjecte(name,OT,Deadline,conn, cursor):
        url = "https://app.liquidplanner.com/api/v1/workspaces/204560/projects"
#       "{\"project\":{\"name\":\"POSTMAN\",
#                    \"custom_field_values\":{\"Fase Proyecto\":\"\",\"GIP_ProjecteId\":\"678\"}}}\n"
        payloadDict = {"project":{"name":name,
                                  "custom_field_values":{"GIP_ProjecteId":OT},
                                  "promise_by":Deadline
                                  }
                        }
        
        txtpayload=json.dumps(payloadDict) 
        print(txtpayload)
        headers = {
                'Accept': "application/json",
                'Content-Type': "application/json",
                'Authorization': "Basic WGF2aWVyLmNvbmVzYUB0ZWNub21hdHJpeC5uZXQ6MDAwMDAwMDA4",
                'cache-control': "no-cache",
                'Postman-Token': "1a97252a-e599-4ec1-9b3a-551319eee74b"
                }

        response = requests.request("POST", url, data=txtpayload, headers=headers)
        responseDict=json.loads(response.text)
        print(responseDict['id'])
        query="Insert into LQ_PI_Project"
        query=query+" (LQ_ProjectId,GIP_ProjectId)"
        query=query+" Values('"+str(responseDict['id'])+"','"+str(OT)+"')"
        cursor.execute(query)
        conn.commit()
        
    @staticmethod 
    def connGIP(conn,cursor):
#        conn = pyodbc.connect(r'Driver={Microsoft Access Driver (*.mdb, *.accdb)}; '
#                      r'DBQ=C:\LQ\LQ_GIP_Integration.accdb;')
        taula={}
        registro=[]
        header=['lngCodiProjecte', 'lngCodiTasca','strDescripcio', 'sngDuracioVT','sngDuracioOT', 'sngFalta']
#        cursor = conn.cursor()
        query="Select Id,lngCodiProjecte, lngCodiTasca,strDescripcio, sngDuracioVT,sngDuracioOT, sngFalta "
        query=query+"from LQ_VW_Tasques "
        query=query+"Where lngCodiProjecte=14194"
        cursor.execute(query)
        tables = cursor.fetchall()
        i=0
        for row in tables:
            registro.append(row[1])
            registro.append(row[2])
            registro.append(row[3])
            registro.append(row[4])
            registro.append(row[5])
            registro.append(row[6])
            i=row[0]
            taula[i]=registro
            registro=[]
            #    i=+1
    


        a=pd.DataFrame.from_dict(taula,orient='index',columns=header)
#        print(a.loc[3]['LQ_ProjectId'])
#        print(a.iat['LQ_TaskId'])
#        cursor.close()
        
        return a
    
    @staticmethod     
    def crearTasquesProjecte(LQ_VW_tblTasques):
        url = "https://app.liquidplanner.com/api/v1/workspaces/204560/tasks"
        headers = {
                'Accept': "application/json",
                'Content-Type': "application/json",
                'Authorization': "Basic WGF2aWVyLmNvbmVzYUB0ZWNub21hdHJpeC5uZXQ6MDAwMDAwMDA4",
                'cache-control': "no-cache",
                'Postman-Token': "0646bffd-946b-4234-b8f5-9d5bc3e0f5d7"}
        print(LQ_VW_tblTasques)
        i=0
        fi=LQ_VW_tblTasques.shape[0]
        while i<fi:
#            print(' --: '+ str(LQ_VW_tblTasques.index.values[i]))
#            header=['lngCodiProjecte', 'lngCodiTasca','strDescripcio', 'sngDuracioVT','sngDuracioOT', 'sngFalta']
#            LQProjecteId=LQ_VW_tblTasques.iloc[i]['LQProjecteId']
            LQProjecteId=53563523
            lngCodiTasca=LQ_VW_tblTasques.iloc[i]['lngCodiTasca']
            strDescripcio=LQ_VW_tblTasques.iloc[i]['strDescripcio']
            lngCodiProjecte=LQ_VW_tblTasques.iloc[i]['lngCodiProjecte']
            sngDuracioVT=LQ_VW_tblTasques.iloc[i]['sngDuracioVT']
            i=i+1
            payloadDict = {"task":{"name":strDescripcio,
                           "parent_id":LQProjecteId,
                           "max_effort":int(sngDuracioVT),
                           "custom_field_values":{"GIPTask_ProjectId":str(lngCodiProjecte), "GIP_TaskId":str(lngCodiTasca)},
                                   }
                            }
            txtpayload=json.dumps(payloadDict) 
            response = requests.request("POST", url, data=txtpayload, headers=headers)
            print(response)
            

# invoke the demo, if you run this file from the command line
if __name__ == '__main__':
#    LiquidPlanner.demo()
#    LiquidPlanner.readCSV('TimeTracking_Import - Post_TimeTracking.csv')
#    LiquidPlanner.imputarHores()
#    LiquidPlanner.crearProjecte("project diada ID","7878-2","2019-09-11",conn,cursor)
     conn = pyodbc.connect(r'Driver={Microsoft Access Driver (*.mdb, *.accdb)};'
                      r'DBQ=C:\LQ\LQ_GIP_Integration.accdb;')
     cursor = conn.cursor()
     LiquidPlanner.crearProjecte("14194 - python","14194-1","2019-09-11",conn,cursor)
     a={}
     a=LiquidPlanner.connGIP(conn,cursor)
     print(a)
     LiquidPlanner.crearTasquesProjecte(a)
     cursor.close()
     
     